#!/bin/bash

# File Name:    AddCronJobs.sh
# Description:  This script will append lines to the crontab such that they will
#               Be executed on the relevant schedule
#
# Version:      1
# Auther:       ricky
# Date:         2018/03/16

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

source /opt/capita/innovations/build/options

# Add an entry to update the server operating system and apps every Sunday at 21:00
crontab -l | { cat; echo "0 21 * * 7 apt-get update && sudo apt-get --yes upgrade"; } | crontab -

# Add an entry to update the Capita binaries every month
crontab -l | { cat; echo "0 20 27 * * wget https://bitbucket.org/AdminDogg/innovationslinuxscripts/get/master.tar.gz -P $InniovationsPath && tar -xvf $InniovationsPath/master.tar.gz -C $BuildPath --strip 1 && rm $InniovationsPath/master.tar.gz"; } | crontab -
