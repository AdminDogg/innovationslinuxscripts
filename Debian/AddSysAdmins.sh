#!/bin/bash

# File Name:    AddSysAdmins.sh
# Description:  This script creates a user account, home directory and sets
#               group memberships for each SysAdmin
#
# Version:      1
# Auther:       ricky
# Date:         2018/03/16

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

# Add SysAdmin users, create home directory, set permissions, create file for
# public key and add to sudo group
for User in sa_ricky.beaty sa_richard.wood sa_max.mortimer sa_hugo.raddon sa_adrian.griffiths sa_eddie.williams;
  do sudo adduser --disabled-password -ingroup innosysad --gecos "" $User --force-badname;
  mkdir /home/$User/.ssh;
  chmod 700 /home/$User/.ssh;
  chown $User /home/$User/.ssh;
  touch /home/$User/.ssh/authorized_keys;
  chmod 600 /home/$User/.ssh/authorized_keys;
  chown $User /home/$User/.ssh/authorized_keys;
  usermod -a -G sudo $User;
done

# Write each user's public key into the authorized_keys store
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCZS/mgBMAi7SzG4xFEAvsZ2uWvct5KdslkHlKe7/HI9eIjPVcAsSZse8zJg0yuLwp0uRaAdJJNGR+Av7+S5DhFPVjT07yOeFNvNJk5o7+50TgYsfdwL3djKVSHJRvUeIWHuIj0iDuxYr//PDskvHF01y/Fa+K6JQ+ir2vlIZHIQZAzUq4rZOua/srxbnQcd3fHDASpySRbkwUDWGIlnQZGlhILScOdYYBRcrKn7slO0EHqXx80j+KpA6JlMLYUIvX14ZYtILU4zMZ1al2F5nFeT/LllMYi/lXO7EV4zHSwNMbZIbNzZolp/0z78//Rq/vZtLP94MIHOt4+hNFssWX9Hzve4+guRaaCroISuasr/N6qlo7Uuno2/zCl7RHnVGncwLami7LQ3HkG3m0c5qaSNaXDnIuTXl3n6DaBv3V3chg0ILEqmk9v/gI+bk9wxv8vG1Za7BthWWBiZc+dYFME57iNdgZ05a2KpxZnPOKTjiVBWHlfR+fxSdhlMWIpbtH4a7VbvUKUUIgj1t7Q91riSMW6BRKB1Da9SXm+3kIzLGn7p1l/1K0cnjvHQSFhlH3QCPquLNWlEXim7EX89mqZyvVciNJjlvX2MwL7WnvaKyoLk47qH7bxK9MUDVn1pnqGMr5cWk8HsiQsUiXYcUjwJbmmhGXEh3dtOLTBxkWRtw== InnovationsKey" > /home/sa_ricky.beaty/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCgujpQbaQti/y+qCA9tarPCXCWyvie2Q19OFIY3OXFPUx+5h02q/GOkI1FVHb6oHZUQjd0FnW6aWuiQA3zjcys+tgHoRZGtm5O9rv4W66jA4I/vD1bO2sst6CjJpmDX62SWpGA+absZXsEd2qHaSruX5w755VZHtiMHI12CMVGaRTUszt+SzYbQlRfXdpTyIpuNNJwaRX5pLHPOHAVdMoe6bwRRWbXrRs7ZVVqbkBI+hYSKCdiX8ZtJ45aaCcpBnVD7osZnkV+5nD5ndSXm7Od0pnMXFtBDU0MQCSJKu2bQozVUSDAmf5NxL4iLxZ6ejJfix1oqD+U1sencbKfdv+0t6o1WXk8+h5ceEYVOZaafPxFXRkF0sLJtSPWO52SBMxddakgfCjRP465lqdD6+9OhO/FD+7fIOBRqBe4qp0hODYqnz6TBuUuXTaxmGYsjMFeYciZrRkGsjIGm8MnWkzkI6jWW4Ts7VXBN1qGSLq53ZaWIhxoJ3NLdFsZ7eqHkfYjFMINx7FiaTdkbElvu/trZeF3/1Wt225yRsbGHZejK/xhiUR6kVC8n8K/fcmOleXGAvgGjCNDlMLF/qobdXVMsMxDiDm6+XHoCVRDGiMZed+bzPyyy3prn4X31queSNyujJs4QipRTR4Frl/tgSs7nYW0Ht9ymCKv90gxgterw== InnovationsKey" > /home/sa_richard.wood/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDEEbunRoLkt9W+EPiQcOY7ZWS285pqEOckmcCkKkAW0tSarewV9dcvWVi/iwPHmdME6hbLGMa6G/nnpHySMHTW+PBMpzfaVDPuhov9FLa3LI4SmZoVhDO4D8y++mjxGDwBHkCpLWEW5/td+XFblxn60HVDEQQqodpGmO3vvZZaFn3eIitZ4idXBV4C+eJmVV95hEqG2A11iogqpiGRbswWJoyG8gN1+eB8A8/Ab7cqqBuefUn3kAgAFYbCm/RqXmi9WM222YjSGHB42KwEc7O1yDzMI20OU55yepelBMeye2uyd1Z1XfQq+KdjTi9FivA9mAPkGqFebIvRUdKfRXYjGHnryq/NA+WGCZfszb4gSc4oH6JY9Mn21BwM70C66yRn3hK7DXcebonzIDMRFeqTIvKpb5PtD/clBYH5lw3jyteHHYoCMlwXb5nUBolgf+X+a8I0k8hbTo2vYlETDlT8Bah+7zOIlDIT/n3TzlDBwTrsSlMy6siD1EKyEIdDwmvIZcKIproXzcJajBAUKGJirbEqNvkkVbjflXr0l5NfuT+zPGXpJ1f+QIirgYF78VA1Wl5vWrQm4RjuW32lGJbdU+RtwvqdoHpiyYbmYDnJxVhxP53SBrh85aS3hZH+slEl/0Ih2RibG+brAj87W9VSMMzF8/tbb6X6XHXCqJhXNw== InnovationsKey" > /home/sa_max.mortimer/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC91wL2dx2lvngV7IZoORuzTDFfrXUY3pZC4gl3wBh2oCYfIE1DLm1+CuN54ULCjIdvWaXqf+ZvutaDcCVxzzPr8TaA3BGgG/p47wIGCa5guOX0GmOy8NcdTvPYG1DGJcm89K8DZerlx3NMdqqpXS/I6h7XpntyCDiS2d/bj+3MyE+WAuebg2Xmxmw3ycFxR/UrVnAkQJaUe5cEO6li2WPS5r0dcFrJXaMp022lAW2GxuWREY62czElaL3iRgwXQzs7axEKRg9TJrYMyuU9kFEpNAv+ipAXtsRJJSO36aaSY13P0/Y/LOvFVhb15hubxjturDY9Hzk8IGA0Fq0nM1Em6dfDiGyfKTG0A8Y3V3b+HuVXmDB5FhU81iTh+tjRq61fJioseNjOygImwglUtdcnsm/QpbyB3X70MiyostiAMVCkILPw1iu6X8RyX1MoR7DpdwPcUNcw9GYmWY3qQRYiERCuiWRbTwLSf43xM7fCNFtlfi/MwlORzZwbOnHXb+bI56ts6JgSHJvJO/4/PoR3QyPDC6q6xGZte/AHzXV9HQKrLAFCjxk8r10fNpyPIcQBFiC8nEUqz+EKlwKS9K25GOAs3P5YDv45iXDLz5Y/a4oyJnkZogOOvJ3eRGpVL5hyI98rXhl+giDq9ZqbM2UgChnXSJaAx52B/zEkAR+Unw== InnovationsKey" > /home/sa_hugo.raddon/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDAeLyr3WIp2KTj0djscRjiBzYZGTHm3809kfx/8G8ehLsM+vgu76YfrsWgBTzQRxqDxW+xHpk3BsHgHloAjiEz9NqT499z0kWgid5R4AlCZ6N9K1n3xK1uKnO2YSQi5bpsxknJi/FwIFJsoE7+m7hqf1WdrYLPX7zENoHElvHa+dDd2X4j4+H/O9MiFSjs0birHk23djsQbodAId4iOL1lLfw1DanUcKXJqi/65IoHCq/+faRxb+wvF+w3Nx9Es35nPwh6jjLtVQJlc8WTDnqvtHVPnJRkd/xHEIMk16rHiC2Oq1vp7cI4brJpg23gkAxMhbb7bxvqTTzoUQtvUvKSuk9aHwJ2WKEOfp51L2uukhBpgQyQjWnmXdyvbkkJLFk2jV7bxvmmH3Dh+uBcz/ifQ03sAtfDiXdQDbDcNGGOxzmrqWTZVq2cRaL7XHboSLkn+k5s5EBJzROwmlT50ZeQ0d/83mHcBme2fTKF79sQY4/JlyoJ4IAI8txaOWPj/C3fqlD4V3lKN50yLTFgDEguksx12fEBqDaH2kV3+Q3r6x2a5Ey8cbrZuUb2FqhyLybApJ0ExofIANTq82wfYjK3g1HpnuxPlFJHl9GPV+beD+eRj04oxnWaWEY9fpRfo9aoqa8YNN498GIMzuYPe65ELd6HEFaZoTHf0vFQuVyo1w== InnovationsKey" > /home/sa_adrian.griffiths/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCUTpExqsgm12Y1sBSfXeqiPOA57dlZqWMD9DZYPajQgk6h4chL7bUS9S31tmdnP4CTywSfaNHDC6BdSB0lb3M83Q1bsC0iK3zgsaUJ2Lu1uw+DnLJSP3904pDn24uz0kypG+hQUTxCCXy5jlniaWO6MKtQo9Y4/TBk6YDF1vSqxXu2HnwjVPbX9AvFPyOe9wd8k6VZ7WXPYgm75uhBqvsd2RO00tG5QhRBpPZ9eZAs7LyMbfAx0Dj7lvpIsvvSDF/kvrdlB2UCGRL+Dy2Y4JP2zGeTKaJ6A+aTbfqVTPK8yQCR1e7mml+aEUBlaZZEqeVOGXU4nidNWnzXqSDj3qus1EMCyTnmYOA0D8ErfenkKwwsKwSOXH0OW73rXWE0vXZI7VLvZftzUfCRu0CgS/ZVGo2o0k6iWqJposNrk3EbNy+8hR1PYrDhmT1FAF/5AlcgR3u/OC6//qoiM0pD0/IloNS6jtdPNp3HRbaoBnp/OvbWPU7KAYcv4qLoGW7tjlqm6rgBWvao7h9wCj/T5Xk/idOzne/+rNhDyAuAJgOKlaOi41T7XbVx8IJHhCIitP6wl3Ppl0c00oBRvrxd5rTCfm8HDrO6uGpDNlEA2Jb4S2l4Cs1Bk1EasDgk52yUP8YitVlpq5LT1xK5WxqAQohEgyuSeAwWhhdQHaqg4LEYw== InnovationsKey" > /home/sa_eddie.williams/.ssh/authorized_keys
