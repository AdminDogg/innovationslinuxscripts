#!/bin/bash

# File Name:    RunFirst.sh
# Description:  This script accepts input from the options file and
#               configures the server apropriately
#
# Version:      1
# Auther:       ricky
# Date:         2018/03/14

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

source /opt/capita/innovations/biold/options

# Check to see if we are running as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
else
  # Create user groups for Innovation teams
  # groupadd -g Group_ID Group_Name
  groupadd -g 1001 innosysad
  groupadd -g 1002 innodev
  groupadd -g 1003 innotest
  groupadd -g 1004 extdev
  groupadd -g 1005 monitor

  # Set permissions for Capita directory
  chown -R root:innosysad /opt/capita
  chmod -R 770 /opt/capita

  # Add SysAdmins
  $BuildPath/Debian/AddInnovationsSysAdmins.sh
  $BuildPath/Debian/AddInnovationsDevs.sh

  # Disable sudo password requirement for innosysad
  echo '%innosysad  ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers

  # Disable password authentication
  sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config

  # Install nano because vim is terrible
  apt install nano

  # Add scheduled tasks
  AddCronJobs.sh

  if [ "$IsWordpressAppliance" -eq 1 ]; then
    $RunPath/WordpressAppliance.sh
  else
    echo "Skipping WordPress appliance install"
  fi

fi
