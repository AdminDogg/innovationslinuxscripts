#!/bin/bash

# File Name:    AddInnovationsDevs.sh
# Description:  This script creates a user account, home directory and sets
#               group memberships for each Innovations developer
#
# Version:      1
# Auther:       ricky
# Date:         2018/03/16

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

# Add Innovations development users, create home directory, set permissions,
# create file for public key and add to sudo group
for User in sd_matthew.feeney sd_andrew.trowles sd_david.hathaway sd_gavin.trowles sd_simon.pocklington sd_stuart.macfadzean;
  do sudo adduser --disabled-password -ingroup innodev --gecos "" $User --force-badname;
  mkdir /home/$User/.ssh;
  chmod 700 /home/$User/.ssh;
  chown $User /home/$User/.ssh;
  touch /home/$User/.ssh/authorized_keys;
  chmod 600 /home/$User/.ssh/authorized_keys;
  chown $User /home/$User/.ssh/authorized_keys;
done

# Write each user's public key into the authorized_keys store
echo "sssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0AjNgMiZi/mMpwGDt9SC0hFZkDj/YYhkezkxFAISMOfNzai1B786d+0kToxTlxftCy/wdB4cyWwyJMSSjDZkP49c8/DJSFLj+nmMLyPGj8Hbtoa+akl/xesEp+lxTdzM0uY8vWo54d8XJeANNjxzrYa8eXaeR5xkGHagBi4maxr0fY+DNgzet0/GdjpCeQ/AArTC85lyCZsOp2LBNjvYF49Q3pAiZb9fGCp5Yi0wc3kPMXEj2vRiIQLv8MkWPaef72OgI6SwJv5VrourP+Bg/qOeDE63OGQnL/8SgKTju6AkKIPXl6lYbzz8uAdHhO2nftjamUeONf6khy/wkYQrFFHGKo5WTgBF2UYtNPzo+yspyIDqB9aMzhLSKmnHDFCJGRYyP427jaj8UVdVD95+MWkyKDCc3/TieyQmVG1qP5ullhjd/7kqgA65UQNFWY6BXoRK7EmUgu9bpZ7Jzn2jxM6P7rlwQMnPCSpW8GtbA/5XB8ZasBqzlMb0Ik+/17EuBHU8WZT8Qn4baqy/lIvFTnapRZeM4haNyefscxN85xiy9EnmPd71zSQrbEc2t55fRbyq3/KN3Gs6BEDPiJmYqFwimNIdg6v7UFY/xsxEqO9ORntb5UehmhXDnBYfBQXycUcqWuOl0DdiZ8AD7EDjC4h4AuewAWJX/34kLg56z9w== InnovationsKey" > /home/sd_matthew.feeney/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDsT7p2Iq9cz9Gd1PzO5fvzXUIiFuYYJ2wdU5ajObZEFkcE3gWP9yfxFVJS43aifpsQvxuHVD+OBa5aq2kTnOOuluhnEeP94rKy3ALuI48kULpfCCCY+YcyV7ZvT+/r4njmf/uBWjwvmmZKffexFgXSnXdk+BTl3r6e58SEmAFq3p0D6EYq47F7/j+xTn1Hv12so6y6GX3u2JG1n4yLNK4+gx4+xny8Z9snuw73ET7wBMeisnJoFzBnp4H71Zch43z+M0489ES0Bw5HKSeqNl4CHeXN/6wc/gkbLLUObO8ypMzR8BbEMbM9YWdvY+zqD/YgMgis2nSHW0A4oSb7Wymam8DBCm2Xt9onjNSPP8ruB5XGD9zPcvlGUkj+dT7qxA4aGsf/hLxYfdoIwQA2JJjReUm6uHgjlE156ed4rEcyMU+Sib+nEUioY9J4QcdlQpG/8KR20KfhO2QddIQtIWGH5lhNX7C1/VmOqCEFNA8gdKACJr53NEbqoYtd6YeGfsUePbT1RLkFiyPV5RfiUULjzNesp0KaxxCUwA5xGbi3gIC6BoxYhAzr6R6rFFsPlx5OFUu8DAJSejDYdSoYTBlNdMoACginbWk1FM4jEwyT9XUaox7oCnXlu7MzJ2bvH7R/9rLHl8CrdyiA52vG5rTQhuNVnL1Jm+SC/NZnIDD2uQ== InnovationsKey" > /home/sd_andrew.trowles/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCjiTmqnTQxO9SoH+TGnl6sI6R2m1JGfxXJuRM5NXQiLmgKBNvfAJWUlHWqsndRVSebzokMPLDlowuK/DUhXv1aOoIql4eftJMEr5auF40thAx1DZhj3CkIbqO8CyoE2coUn8qKQlYMK7M/y1Yj/WJzDiaNYhlamVa4ThEqvmIUhT/PKAatbf5fYqTPuGePE15bMccU7hSBWI59kBAuw7nH9fSJdyRsqvek3/jHdYPg/WUTqPXsPYlQ9TtP6LWRkj3Be6ZrwcA+QLevzby5fBGdmeJXemGZ1H2jDGRS/8/ECunXPaT0EzhndtmxwoGUQygcN/ZJKggcJZfpFgT2I+Ap5fuoS1uvOdqLF/XjdL+RU2ktijCKBBaaNqI9taSKF/nQDg0KAeePpA/QCdwU+u+dMLXqNXSZATkf/n/yy480o6OxuJb+9Gob24VcBp9sUf9Kg22x3Di/t0SddW9cBTaMYCnNIkMj3vLyc1EIm+L2r72aapdvIi1boauE5ru3tlwEUm78HXl6uL1/Jtx/0i18gCsbabhb1SNSAgOe5mup1aMzfAfMTwbisMRadW8mSV+Vx/WqOpwv1oYRjeUAj9AbL+UKKu9DCuYR6k18qKKqJMY11YNi/mcG2MvVqXtHCAEDJhwQ6fzpXCU/gIhVZ0AswCcH3U89iEm7Bvlo7mOYkw== InnovationsKey" > /home/sd_david.hathaway/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCsqo9mB0G+E7OHGgAzI39i39IcLBFUPlvAEO6aFyACcWaVXrq7NjyTh5UIuHpElVgjQe3iCEunb/UYbVQc4kyVtRglqaNReGuviTkhC+ARzNzQ5pOdSXaTD8TOVXM3QWThngzyy+yZ09x3do43ePS8HR70jveICbiZJR3YFxMqXCqYgwOyj8hqniEqZbQ/0l10pCIrAMYL9X3zD0p3qjY2ZIndSCBAYmhBVYQIx2jxZm8yU/0J9qHo2KnOSFxW+zxVx58M38gXS+3Onhx3RvvTctbZF3Z4MJeHATkyYnPLklVmUACaqenzfv4Y9XQiPOoiacBmeJD/JUzXeAs/hP/fYmK/0Sj/rS49zzDx8r+39LlBkFqa0GwpSEvJsjo/bHoYPJeo429F21rW8lVO75DBDV0znbxVzUD1H3nateIrJQ9gOW97A+8Aqd8DuYRLtdj40IFFaltocb9s6kJOI+xt8mhNRwI1mcT0exVlgs4+syXF64qu4zmT9f9R4PeLK1SZkPc4JdKGq182AJPO8PEaPslJ/MWg4l3oVVOHImG64VmbCIXocpUX/x0c9bUVFjWc7PoF9N5x1pJDddiED/GVYAcp7qzU4trebTPbLuT463/470QHXCn2Jx1XmxYFIL8NXwFNy+Nk7ktNVaCm4lAQsPc70xglnkvYeJ9llktIMw== InnovationsKey" > /home/sd_gavin.trowles/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0Yw+XRsBsFzL4kCrbmcWxgBgyyib9BgiOluwK2eAqdTEcprM5n1pXwlXQbBJqYBmXqH1tzX22eAHLtlVV8D2V/6gmmHPfisM0hedtTsGnz4xP4xuHluOcUo3v7fRYGRPvFSo7xpy3cZoUjYCYAaO9IKU00r54DP1WQtm3ZM1HZ3zUDqaKFO5oi4zsh7RzQ10sq7C6LaNeGFU4vhrJoBMu4xv1puaDBuyKG/rp2plYaJ2sH6EwQCAJQwaK+yYcf5E01pScY/a8IPd8P23EzMDA5Kn6E1+Ga9GksK2HO63plHUI17EQHv6Idtr4ulS/fLhghkC8xUyI7ckB22wmYNzAM/gAab8B1sXX2q+IwfSE3DVKR43JdYHejqL5l6ZG/I6OUPcGdNqgbLPq2DFmJ/OzJDyi4mN1NPuufDqqbWx3kVXTi+WHlUQD/vZd6CKeVDA3n6810+9adwVSpPg16FrdnbBh+1dikPGgmJvWvNc9m8+BSs3w7QwHtPAWL0OZBNwLy0hnLzOtp8P27XsOH4cnlemfcFsVsLlpEjtSnr8bBYFntyTbvjljAAzxxzV8FwnnjY12zw/mdpt6Thp5Q25cARSdH7QoOW9l1F+3IPw1MyVSaUEGivwKTCShZgJKvyVsn0i0SzMA5D5ThCEN5qsfvvAVqi//YZADu94zR5R4mQ== InnovationsKey" > /home/sd_simon.pocklington/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDqsuL9e4BfVkJT+E5nJjfLuO3SV269/4NPzUvtwskbZ28hqznnpssbJ4Z+gzvVf2XrqMZbp496U2e2/QG5rR3yLUfyEM6VeqIDIqjESZ32NJ8zTnK0tv/Ypj3/9jSc2s/lEOC3nXoCkdH9/qJzqFmZeyXIIQTAwRKUYghRFzw4Iq63B3uMQPV1O0t+TArvgvi6d2gH1UfuvoLE5tToJEEyxMISD4sBkpjT2KOhqvmeEroxDitxo2Jjqq4fyxAQ8czssQZIaLTLiMZJC5m5A+CMH/q5ZD5Ml02z4rNibX3QwfPdWo7pS1rao12WYlXG5f9H90FmBEuqkXzGlXUAMhKBnPUuPcfMbUsCFCWF4Vi79d0W3VWQi95iyFzmDa46jM+rtFYXbdQOA5A0/RzXBg34jHeAOSuaHlmYOGx/ALfO6YaX78D4rw5QFpLVFYGyqGBJf6A4/Fm618C6E4bNkx9IdlPNFvvs8hLI0BXZ0PmS3tWPoTEvVKiRU7a05gtWQCmPAtLsNUL8iL0pczo2s46hezMtHZ/H8kAeurmTHMUM5CH5kTikKw7livENPfkRrHazd0Kmn8H0kYnk1Wj2t9bvn2WdUvO8T1eOlh4HxmYxZzF/1CqMWpVixq/B2JuerfNseJE9eBnyTRBgeGcWDAb3jFbtx1Z6Q5q+MipLZvR2AQ== InnovationsKey" > /home/sd_stuart.macfadzean/.ssh/authorized_keys
