#!/bin/bash

# File Name:    WebServerBaseInstall.sh
# Description:  This script sets up Apache, PHP and supporting extensions for
#               a standard Innovations web server
#
# Version:      1
# Auther:       ricky
# Date:         2018/03/17

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

# Install Apache and supporting extensions
sudo apt-get --yes install apache2 php libapache2-mod-php php-mcrypt php-mysql \
php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc

# Add the ServerName and .htaccess overrides to the Apache config file
echo "\n\nServerName $HostName\n\n" | sudo tee -a /etc/apache2/apache2.conf
echo "<Directory /var/www/html/>
    AllowOverride All
</Directory>" | sudo tee -a /etc/apache2/apache2.conf
sudo systemctl restart apache2

# Open enable Apache firewall rules
sudo ufw allow in "Apache Full"

# Amend apache configuration to prefer php files over html files
sudo sed -i "s/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm/g" /etc/apache2/mods-enabled/dir.conf
sudo systemctl restart apache2
