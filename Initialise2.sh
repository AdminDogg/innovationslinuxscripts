#!/bin/bash

# File Name:    Initialise2.sh
# Description:  This script is run after Initialise1.sh, as root, and completes
#               the base configuration of the new server.
#               main.sh follows this script.
# Version:      1
# Auther:
# Date:         yyyy/mm/dd

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

# REMEMBER TO TUN THIS SCRIPT AS ROOT

#Set paths
CapitaPath="/opt/capita"
InniovationsPath="/opt/capita/innovations"
BuildPath="/opt/capita/innovations/build"

# Delete build directory if exist, then create fresh one
if [ -d $BuildPath ]; then
  rm -R $BuildPath
fi

mkdir -p $BuildPath

# Download and unpack scripts from BitBucket
wget https://bitbucket.org/AdminDogg/innovationslinuxscripts/get/master.tar.gz -P $InniovationsPath
tar -xvf $InniovationsPath/master.tar.gz -C $BuildPath --strip 1
rm $InniovationsPath/master.tar.gz

# Make the scripts executable
chmod -R +x $BuildPath/*.sh

# Call main script to configure the server
$BuildPath/BaseSetup.sh
