 _____                            _   _                 
|_   _|                          | | (_)                
  | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
  | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
 _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
 \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

This set of scripts is used to build all Linux servers hosted by Capita
Technology - affectionately known as Innovations.



Change log
2018/03/14      Repository creation and first development


PREFACE
This information is intended to be used by Capita Technology SysAdmins to help
homogenise the deployment of new Ubuntu servers to the Innovations network.
This pack of scripts is designed to be run from a terminal session connected to
the target server via SSH.
The first script to run is the Initialise1.sh file, which you will need to have
a local copy of in order to execute the commands therein.  The server will
reboot without warning after running this script.

INSTRUCTIONS
Connect to the server for the first time using the username you defined during
the setup process.
Elevate to the root user using the following command;
  sudo su
After you re-enter your password, the bash prompt will change to indicate you
are now executing commands as ROOT.
Copy the text from Initialise1.sh and paste it directly into the terminal window
After the server reboots, perform the same process with Initialise2.sh.

When that is complete, the Capita binaries directory will exist at
/opt/capita/
Edit the /opt/capita/innovations/build/options file to define which software
will be installed on the newly provisioned server.
Save the options file and run /opt/capita/innovations/build/[distro]/RunFirst.sh


GROUPS
innosysad:      Innovations SysAdmin team
innodev:        Innovations Developers
innotest:       Innovations Testers
extdev:         External developers (CSS, for example)
monitor:        Account used for monitoring solutions
