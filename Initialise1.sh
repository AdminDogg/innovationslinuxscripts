#!/bin/bash

# File Name:    Innitialise1.sh
# Description:  This script is to be run manually, as root, on a newly
#               provisioned Innovations server
#               A server reboot is forced
# Version:      1
# Auther:       ricky
# Date:         2018/03/14

#  _____                            _   _
# |_   _|                          | | (_)
#   | | _ __  _ __   _____   ____ _| |_ _  ___  _ __  ___
#   | || '_ \| '_ \ / _ \ \ / / _` | __| |/ _ \| '_ \/ __|
#  _| || | | | | | | (_) \ V / (_| | |_| | (_) | | | \__ \
#  \___/_| |_|_| |_|\___/ \_/ \__,_|\__|_|\___/|_| |_|___/

# REMEMBER TO TUN THIS SCRIPT AS ROOT

echo "
http_proxy=\"http://TMG01:8080/\"
https_proxy=\"http://TMG01:8080/\"
ftp_proxy=\"http://TMG01:8080/\"
no_proxy=\"localhost,127.0.0.1,localaddress,.localdomain.com\"
HTTP_PROXY=\"http://TMG01:8080/\"
HTTPS_PROXY=\"http://TMG01:8080/\"
FTP_PROXY=\"http://TMG01:8080/\"
NO_PROXY=\"localhost,127.0.0.1,localaddress,.localdomain.com\"
" >> /etc/environment


# apt and some other core programs don't use the global proxy settings, so we
# set them inside the 95proxies file
touch /etc/apt/apt.conf.d/95proxies

# Squirt config into 95proxies file
echo "Acquire::http::proxy \"http://TMG01:8080/\";
Acquire::ftp::proxy \"ftp://TMG01:8080/\";
Acquire::https::proxy \"https://TMG01:8080/\";" >> /etc/apt/apt.conf.d/95proxies

reboot now
